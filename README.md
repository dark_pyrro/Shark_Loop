Obtains information about networks to which the Shark Jack Cable is connected and the idea is that it will loop "forever" so that more than one Ethernet jack can be examined without rebooting the Shark for each new Ethernet jack.


Targets Ethernet based IP networks that hands out DHCP leases to connected clients (and has no 802.1X protection, or such).


The payload mainly targets the SJC (Shark Jack Cable) but should also run on the battery based Shark.


It's really a kind of 0.00000000001 pre alpha version, but I had the "embryo" laying around so I thought it would be better to "put the boat in the ocean" instead of trying to sand the corners down before release. So, not really pressure tested, but should work.


Cloud C2 exfiltration isn't tested or ready either. I any way, it will always depend on what the target network allows. Also remember that it adds additional "fingerprints" to any red team engagement.


Should be safe to use, the only thing that I can see happen to the Shark itself is if it's pulled from the power source when writing to the file system. If "halting" the Shark when the LED blinks blue, the risk of that happening should be less though.


NOTE! Use at your own risk!
